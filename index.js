const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = process.env.port || 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));
// app.use(cors());

mongoose.connect('mongodb+srv://admin:admin@prodmongodb.iaelz.mongodb.net/course-booking?retryWrites=true&w=majority', {

		useNewUrlParser: true,
		useUnifiedTopology: true
})

let db = mongoose.connection; 

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Successfully connected"));

app.listen(port, () => console.log(`Server running at port ${port}`));